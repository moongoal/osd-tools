OSD TOOLS
==================

Tools for managing OSD in GNU/Linux.

Requirements
------------------

* GNU Make
* GCC
* libxosd

Currently available OSD tools
------------------

* osd-volume: Display volume bar on screen


License
------------------

All sources are published under the MIT opensource license. Have fun.


Disclaimer
------------------

This repository contains code that has not been tested and has been written in minutes. Watch out for bugs.
