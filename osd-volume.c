/*
  Copyright 2017 Alfredo Mungo
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xosd.h>
#include <signal.h>

xosd *osd = NULL;

void sighandler(int sig) {
  if(osd) {
    xosd_destroy(osd);
    osd = NULL;
  }
}

void setup_signals() {
  struct sigaction action;

  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = sighandler;
  
  sigaction(SIGTERM, &action, NULL);
}

void help() {
  fputs("Usage: volume 0-100", stderr);
}

void show_volume(int volume) {
  osd = xosd_create(1);

  xosd_set_font(osd, "-*-fixed-*-*-*-*-24-*-*-*-*-*-*-*");
  xosd_set_colour(osd, "Red");
  xosd_set_timeout(osd, 1);
  xosd_set_shadow_offset(osd, 1);
  xosd_set_bar_length(osd, 100);
  xosd_set_pos(osd, XOSD_bottom);
  xosd_set_align(osd, XOSD_center);
  xosd_set_vertical_offset(osd, 100);

  xosd_display(osd, 0, XOSD_percentage, volume);
  xosd_wait_until_no_display(osd);

  if(osd)
    xosd_destroy(osd);
}

int main(int argc, char **argv) {
  if(argc == 2) {
    const int vol = atoi(argv[1]);
    
    setup_signals();
    show_volume(vol);
  } else {
    help();
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
